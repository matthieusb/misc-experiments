(ns clojure-word-count.core
  (:require [clojure.string :as string])
  (:require [clojure.java.io :as io])
  (:gen-class))

(defn countNumberOfWordsInString
  "Counts nnumber of words in a given string"
  [givenString]
  (count (string/split givenString #" ")))

(defn getAllTextNumberOfWords
  "Counts number of words in example.txt file and returns it"
  []
  (println "Starting word count of example.txt")
  (def numberOfWords 0)

  (with-open [rdr (io/reader "resources/example.txt")]
    (doseq [line (line-seq rdr)]
      (def numberOfWords (+ numberOfWords (countNumberOfWordsInString line))))) ; Maybe make something more idiomatic if possible
    numberOfWords)

(defn -main
  "Just the main function"
  [& args]
  (println (str "Numnber of words found : " (getAllTextNumberOfWords))))
