(ns clojure-word-count.core-test
  (:require [clojure.test :refer :all]
            [clojure-word-count.core :refer :all]))

(deftest a-test
  (testing "Testing our counting function"
    (is (= 12 (getAllTextNumberOfWords)))))
