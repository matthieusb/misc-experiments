# clojure-word-counter

Very Simple word counter in clojure, read from *example.txt* situated in resources.

## Usage


Or install leiningen and just run :

    $ lein run

Or just execute the jar :

   $ java -jar clojure-word-count.jar

## Examples

The example file in the resources folder contains 12 words.
Tested with this website : [word counter](https://wordcounter.net/)

## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
