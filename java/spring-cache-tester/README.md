# Spring Cache Tester

This project goal is to study some simple Spring's Cache annotation behavior.

## Problem

Make a small Spring Boot application with an _@Service_ and a **methodA** calling a **methodB** who is _@Cacheable_. 

The call must make a cache hit and you must explain how this is working (or not and make it work)

## Spring documentation

- Spring boot caching features:
https://docs.spring.io/spring-boot/docs/2.3.4.RELEASE/reference/htmlsingle/#boot-features-caching
- Spring reference cache: https://docs.spring.io/spring-framework/docs/5.2.9.RELEASE/spring-framework-reference/integration.html#cache

## Solution

### Results

- A cacheable method called from the exterior of the service directly populates the cache
- A cacheable method called from another method inside the service (called from the interior of the service) does not populate the cache. So it does not make a cache hit when re-called

### Explanation

The cause is simple, spring @Cacheable uses the proxy pattern to make its magic work. So if you call it from inside the service, the generated proxy method to populate the cache cannot be triggered.

Source:

From https://code.google.com/p/ehcache-spring-annotations/wiki/UsingCacheable

(See this Stack overflow question's answer: https://stackoverflow.com/questions/16899604/spring-cache-cacheable-not-working-while-calling-from-another-method-of-the-s)

```
Only external method calls coming in through the proxy are intercepted. This means that self-invocation, in effect, a method within the target object calling another method of the target object, will not lead to an actual cache interception at runtime even if the invoked method is marked with @Cacheable.
```

### Solution

How can we make it work ?

Well in a a real-world context, avoid having to use internal methods when using the @Cacheable method would be ideal.

One solution is the following:
- Add AspectJ and AspectJWeaver dependencies to the project
- Add the `@EnableAspectJAutoProxy(exposeProxy = true)` annotation to the main class

This will use AspectJ proxy and make it public so that you can manually access it in your Spring Beans (hence the exposeProxy property)

- Make the internal call to the method manually call the current proxy with the `AopContext.getCurrentProxy()` method.

This way, the method call will go through the proxy even from the Bean internal "point of view", and make a cache hit as expected.
    
