package com.experiment.springcachetester;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SpringcachetesterApplication.class)
class CachedMehodsServiceTest {

    private static final int EXPECTED_SUM_RESULT = 499500;

    @Autowired
    CacheManager cacheManager;

    @Autowired
    private CachedMethodsService cachedMethodsService;

    @BeforeEach
    void setUp() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> {
                    Optional<Cache> cache = Optional.ofNullable(cacheManager.getCache(cacheName));
                    cache.ifPresent(Cache::clear);
                });
    }

    @Test
    void should_throw_exception_when_sum_limit_is_incorect() {
        // Given
        Integer sumLimit = -1;

        // When Then

        assertThrows(IllegalArgumentException.class, () -> cachedMethodsService.methodA(sumLimit));
    }

    @Test
    void should_calculate_sum_correctly_when_directly_called() {
        // Given
        Integer sumLimitOne = 1000;

        // When
        Integer sumOne = cachedMethodsService.methodB(sumLimitOne);

        // Then
        assertThat(sumOne).isEqualTo(EXPECTED_SUM_RESULT);
    }

    @Test
    void should_calculate_sum_correctly_through_methodA() {
        // Given
        Integer sumLimitOne = 1000;

        // When
        Integer sumOne = cachedMethodsService.methodA(sumLimitOne);

        // Then
        assertThat(sumOne).isEqualTo(EXPECTED_SUM_RESULT);
    }

    @Test
    void should_make_a_cache_hit_when_calling_methodB_directly() {
        // Given
        Integer sumLimit = 1000;

        // When
        cachedMethodsService.methodB(sumLimit);

        // Then
        Cache cacheMethodBCallWithSumLimit = cacheManager.getCache(CachedMethodsService.CACHE_NAME);
        assertThat(cacheMethodBCallWithSumLimit).isNotNull();

        Cache.ValueWrapper cacheValueForSumLimit = cacheMethodBCallWithSumLimit.get(sumLimit);

        assertThat(cacheValueForSumLimit).isNotNull();
        assertThat(cacheValueForSumLimit.get())
                .isNotNull()
                .isEqualTo(EXPECTED_SUM_RESULT);
    }

    @Test
    void should_not_make_a_cache_hit_when_calling_cached_method_through_another_method() {
        // Given
        Integer sumLimit = 1000;

        // When
        cachedMethodsService.methodA(sumLimit);

        // Then
        Cache cacheMethodBCallWithSumLimit = cacheManager.getCache(CachedMethodsService.CACHE_NAME);
        assertThat(cacheMethodBCallWithSumLimit).isNotNull();

        Cache.ValueWrapper cacheValueForSumLimit = cacheMethodBCallWithSumLimit.get(sumLimit);
        assertThat(cacheValueForSumLimit).isNull();
    }

    @Test
    void should_make_a_cache_hit_when_calling_cached_method_through_another_method_that_calls_the_proxy() {
        // Given
        Integer sumLimit = 1000;

        // When
        cachedMethodsService.methodC(sumLimit);

        // Then
        Cache cacheMethodBCallWithSumLimit = cacheManager.getCache(CachedMethodsService.CACHE_NAME);
        assertThat(cacheMethodBCallWithSumLimit).isNotNull();

        Cache.ValueWrapper cacheValueForSumLimit = cacheMethodBCallWithSumLimit.get(sumLimit);
        assertThat(cacheValueForSumLimit).isNotNull();
        assertThat(cacheValueForSumLimit.get())
                .isNotNull()
                .isEqualTo(EXPECTED_SUM_RESULT);
    }

}
