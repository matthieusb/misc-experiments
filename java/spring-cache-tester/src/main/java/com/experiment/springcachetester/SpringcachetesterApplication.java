package com.experiment.springcachetester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableCaching
@EnableAspectJAutoProxy(exposeProxy = true)
public class SpringcachetesterApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringcachetesterApplication.class, args);
	}
}
