package com.experiment.springcachetester;

import org.springframework.aop.framework.AopContext;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.stream.IntStream;

@Service
public class CachedMethodsService {
    public static final String CACHE_NAME = "computation";
    private static final int MINIMUM_SUM_VALUE = 1;

    public CachedMethodsService() {
    }

    public Integer methodA(Integer sumLimit) {
        return methodB(sumLimit);
    }

    public Integer methodC(Integer sumLimit) {
        return ((CachedMethodsService) AopContext.currentProxy()).methodB(sumLimit);
    }

    @Cacheable(value = CACHE_NAME)
    public Integer methodB(Integer sumLimit) {
        if (sumLimit < MINIMUM_SUM_VALUE) {
            throw new IllegalArgumentException("Number should not be under 1");
        }

        return IntStream
                .range(1, sumLimit)
                .reduce(Integer::sum)
                .getAsInt();
    }


}
